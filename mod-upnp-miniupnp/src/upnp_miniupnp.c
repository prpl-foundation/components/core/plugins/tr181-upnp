/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <netmodel/client.h>
#include <debug/sahtrace_macros.h>

#include "upnp_miniupnp.h"

#define ME "upnp"
#define string_empty(X) ((X == NULL) || (*X == '\0'))
#define DELAY_RESTART 2000

static amxm_module_t* mod = NULL;
static amxp_timer_t* restart_timer;
static amxp_timer_t* reload_timer;
static bool alt_com = false;
static char* ext_ifname = NULL;
static char* ext_ifname6 = NULL;
static char* listening_ip = NULL;
static int bitrate_down = 1024;
static int bitrate_up = 512;
static int miniupnpd_port = 5000;
static char* miniupnpd_uuid = NULL;
static amxp_subproc_t* miniupnpd_proc = NULL;
static netmodel_query_t* lan_query = NULL;
static netmodel_query_t* wan_ip_query = NULL;
static netmodel_query_t* lan_ip_query = NULL;
static netmodel_query_t* wan_query = NULL;
static netmodel_query_t* wan6_query = NULL;
static amxc_var_t* plugin_config = NULL;
static amxc_var_t* external_config = NULL;

static void generate_miniupnpd_uuid(void) {
    int fd = -1;
    int retval;
    char buf[33] = {0};

    fd = open("/proc/sys/kernel/random/uuid", O_RDONLY);
    when_true(fd == -1, exit);
    retval = read(fd, buf, 32);
    if(retval == 32) {
        miniupnpd_uuid = strdup(buf);
    }
    close(fd);
exit:
    return;
}

static int create_miniupnpd_conf_file(void) {
    int fd = open(MINIUPNP_CONF_FILE, O_CREAT | O_TRUNC | O_WRONLY, 0644);
    int retval = -1;
    amxc_array_t* uris = NULL;
    amxc_var_t* backends = GET_ARG(plugin_config, "backends");
    amxc_string_t buffer;
    bool enable_natpmp = false;
    bool enable_ipv6 = false;
    bool allow_reserved_addr = false;

    amxc_string_init(&buffer, 0);
    uris = amxb_list_uris();
    when_true(fd == -1, exit);
    free(miniupnpd_uuid);
    miniupnpd_uuid = NULL;
    generate_miniupnpd_uuid();
    enable_natpmp = GET_BOOL(external_config, "enable_natpmp");
    enable_ipv6 = GET_BOOL(external_config, "enable_ipv6");
    allow_reserved_addr = GET_BOOL(external_config, "allow_reserved_addr");
    when_null_trace(external_config, exit, ERROR, "External miniupnpd config null");
    when_str_empty_trace(listening_ip, exit, ERROR, "listening_ip interface isn't bound yet");
    amxc_string_appendf(&buffer, "ext_ifname=%s\n", (ext_ifname) ? ext_ifname : "");
    amxc_string_appendf(&buffer, "ext_ifname6=%s\n", (ext_ifname6) ? ext_ifname6 : "");
    amxc_string_appendf(&buffer, "listening_ip=%s\n", (listening_ip) ? listening_ip : "");
    amxc_string_appendf(&buffer, "enable_natpmp=%s\n", (enable_natpmp) ? "yes" : "no");
    amxc_string_appendf(&buffer, "pf_allow_reserved_addr=%s\n", (allow_reserved_addr) ? "yes" : "no");
    amxc_string_appendf(&buffer, "ipv6_disable=%s\n", (enable_ipv6) ? "no" : "yes");
    amxc_string_appendf(&buffer, "%s\n", "enable_upnp=yes");
    amxc_string_appendf(&buffer, "%s\n", "secure_mode=yes");
    amxc_string_appendf(&buffer, "%s\n", "system_uptime=yes");
    amxc_string_appendf(&buffer, "%s\n", "force_igd_desc_v1=no");
    amxc_string_appendf(&buffer, alt_com ? "bitrate_down=%d\n" : "bitrate_download=%d\n", bitrate_down * 1024 * 8);
    amxc_string_appendf(&buffer, alt_com ? "bitrate_up=%d\n" : "bitrate_upload=%d\n", bitrate_up * 1024 * 8);
    amxc_string_appendf(&buffer, "port=%d\n", miniupnpd_port);
    amxc_string_appendf(&buffer, "uuid=%s\n", miniupnpd_uuid);
    for(uint32_t i = 0; i < amxc_array_size(uris); ++i) {
        if((amxc_array_get_data_at(uris, i) != NULL) && strstr((const char*) amxc_array_get_data_at(uris, i), "ubus")) {
            amxc_string_appendf(&buffer, "ubus_sock_url=%s\n", (const char*) amxc_array_get_data_at(uris, i));
            break;
        }
    }
    amxc_var_for_each(backend, backends) {
        if((GET_CHAR(backend, NULL) != NULL) && strstr(GET_CHAR(backend, NULL), "ubus")) {
            amxc_string_appendf(&buffer, "ubus_backend_url=%s\n", GET_CHAR(backend, NULL));
            break;
        }
    }
    if((GET_CHAR(external_config, "ext_stun_host") != NULL) && (GET_CHAR(external_config, "ext_stun_host")[0] != '\0')) {
        amxc_string_appendf(&buffer, "%s\n", "ext_perform_stun=yes");
        amxc_string_appendf(&buffer, "ext_stun_host=%s\n", GET_CHAR(external_config, "ext_stun_host"));
    } else {
        amxc_string_appendf(&buffer, "%s\n", "ext_perform_stun=no");
    }
    if(GET_BOOL(external_config, "auto_cleanup_enable")) {
        amxc_string_appendf(&buffer, "max_lifetime=%lu\n", (unsigned long) GET_UINT32(external_config, "max_lifetime"));
        amxc_string_appendf(&buffer, "clean_ruleset_interval=%lu\n", (unsigned long) GET_UINT32(external_config, "clean_ruleset_interval"));
    }
    if(!string_empty(GET_CHAR(external_config, "friendly_name"))) {
        amxc_string_appendf(&buffer, "friendly_name=%s\n", GET_CHAR(external_config, "friendly_name"));
    }
    if(!string_empty(GET_CHAR(external_config, "manufacturer_name"))) {
        amxc_string_appendf(&buffer, "manufacturer_name=%s\n", GET_CHAR(external_config, "manufacturer_name"));
    }
    if(!string_empty(GET_CHAR(external_config, "manufacturer_url"))) {
        amxc_string_appendf(&buffer, "manufacturer_url=%s\n", GET_CHAR(external_config, "manufacturer_url"));
    }
    if(!string_empty(GET_CHAR(external_config, "model_name"))) {
        amxc_string_appendf(&buffer, "model_name=%s\n", GET_CHAR(external_config, "model_name"));
    }
    if(!string_empty(GET_CHAR(external_config, "model_description"))) {
        amxc_string_appendf(&buffer, "model_description=%s\n", GET_CHAR(external_config, "model_description"));
    }
    if(!string_empty(GET_CHAR(external_config, "model_url"))) {
        amxc_string_appendf(&buffer, "model_url=%s\n", GET_CHAR(external_config, "model_url"));
    }
    if(!string_empty(GET_CHAR(external_config, "model_number"))) {
        amxc_string_appendf(&buffer, "model_number=%s\n", GET_CHAR(external_config, "model_number"));
    }
    if(!string_empty(GET_CHAR(external_config, "serial"))) {
        amxc_string_appendf(&buffer, "serial=%s\n", GET_CHAR(external_config, "serial"));
    }
    if(!string_empty(GET_CHAR(external_config, "upnp_amx_wan_interface"))) {
        amxc_string_appendf(&buffer, "upnp_amx_wan_interface=%s\n", GET_CHAR(external_config, "upnp_amx_wan_interface"));
    }
    amxc_string_appendf(&buffer, "%s\n", "allow 1024-65535 0.0.0.0/0 1024-65535");
    amxc_string_appendf(&buffer, "%s\n", "deny 0-65535 0.0.0.0/0 0-65535");
    retval = write(fd, amxc_string_get(&buffer, 0), amxc_string_text_length(&buffer));
    fsync(fd);
    close(fd);
    sync();
    when_true(retval != (int) amxc_string_text_length(&buffer), exit);
    retval = 0;
exit:
    amxc_array_delete(&uris, NULL);
    amxc_string_clean(&buffer);
    return retval;
}

static void start_miniupnpd(void) {
    int rv = 0;

    rv = create_miniupnpd_conf_file();
    when_failed_trace(rv, exit, WARNING, "Didn't start miniupnpd, conf is invalid (at boot it can be because interfaces aren't bind yet)");
    amxp_subproc_new(&miniupnpd_proc);
    rv = amxp_subproc_start(miniupnpd_proc, (char*) "miniupnpd", "-f", MINIUPNP_CONF_FILE, NULL);
    if(rv == 0) {
        SAH_TRACEZ_INFO(ME, "miniupnpd has successfuly started (pid[%d])", amxp_subproc_get_pid(miniupnpd_proc));
    } else {
        SAH_TRACEZ_ERROR(ME, "Couldn't start miniupnpd (%d)", rv);
    }
exit:
    return;
}

static void stop_miniupnpd(void) {
    if(miniupnpd_proc != NULL) {
        if(miniupnpd_proc->is_running) {
            amxp_subproc_kill(miniupnpd_proc, 9);
        }
        amxp_subproc_delete(&miniupnpd_proc);
    }
}

static void lan_intf_changed(UNUSED const char* sig_name, const amxc_var_t* data, UNUSED void* priv) {
    free(listening_ip);
    if((amxc_var_type_of(data) != AMXC_VAR_ID_NULL) && !string_empty(GET_CHAR(data, NULL))) {
        listening_ip = strdup(GET_CHAR(data, NULL));
    } else {
        listening_ip = NULL;
    }
    SAH_TRACEZ_INFO(ME, "LAN interface changed: %s", listening_ip);
    amxp_timer_start(restart_timer, DELAY_RESTART);
}

static void network_ip_changed(UNUSED const char* sig_name, UNUSED const amxc_var_t* data, void* priv) {
    const char* network = (const char*) priv;

    SAH_TRACEZ_INFO(ME, "%s IP changed", (const char*) priv);
    if((network != NULL) && (strcmp(network, "WAN") == 0)) {
        amxp_timer_start(reload_timer, DELAY_RESTART);
    } else {
        amxp_timer_start(reload_timer, DELAY_RESTART);
    }
}

static void wan_intf_changed(UNUSED const char* sig_name, const amxc_var_t* data, UNUSED void* priv) {
    free(ext_ifname);
    if((amxc_var_type_of(data) != AMXC_VAR_ID_NULL) && !string_empty(GET_CHAR(data, NULL))) {
        ext_ifname = strdup(GET_CHAR(data, NULL));
    } else {
        ext_ifname = NULL;
    }
    SAH_TRACEZ_INFO(ME, "WAN interface changed: %s", ext_ifname);
    amxp_timer_start(restart_timer, DELAY_RESTART);
}

static void wan6_intf_changed(UNUSED const char* sig_name, const amxc_var_t* data, UNUSED void* priv) {
    free(ext_ifname6);
    if((amxc_var_type_of(data) != AMXC_VAR_ID_NULL) && !string_empty(GET_CHAR(data, NULL))) {
        ext_ifname6 = strdup(GET_CHAR(data, NULL));
    } else {
        ext_ifname6 = NULL;
    }
    SAH_TRACEZ_INFO(ME, "WAN6 interface changed: %s", ext_ifname6);
    amxp_timer_start(restart_timer, DELAY_RESTART);
}

static void start_netmodel_callback(void) {
    lan_query = netmodel_openQuery_getFirstParameter(GET_CHAR(plugin_config, "ip_intf_lan"), MOD_NAME, "NetDevName", "netdev-bound", "down", lan_intf_changed, NULL);
    wan_ip_query = netmodel_openQuery_getAddrs(GET_CHAR(plugin_config, "logical_intf_wan"), MOD_NAME, "global", "down", network_ip_changed, (void*) "WAN");
    lan_ip_query = netmodel_openQuery_getAddrs(GET_CHAR(plugin_config, "ip_intf_lan"), MOD_NAME, "global", "down", network_ip_changed, (void*) "LAN");
    wan_query = netmodel_openQuery_getFirstParameter(GET_CHAR(plugin_config, "logical_intf_wan"), MOD_NAME, "NetDevName", "netdev-bound && ipv4", "down", wan_intf_changed, NULL);
    wan6_query = netmodel_openQuery_getFirstParameter(GET_CHAR(plugin_config, "logical_intf_wan"), MOD_NAME, "NetDevName", "netdev-bound && ipv6", "down", wan6_intf_changed, NULL);
}

static void stop_netmodel_callback(void) {
    netmodel_closeQuery(lan_query);
    lan_query = NULL;
    netmodel_closeQuery(wan_ip_query);
    wan_ip_query = NULL;
    netmodel_closeQuery(lan_ip_query);
    lan_ip_query = NULL;
    netmodel_closeQuery(wan_query);
    wan_query = NULL;
    netmodel_closeQuery(wan6_query);
    wan6_query = NULL;
}

static int igd_changed(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    bool enabled = GET_BOOL(args, "enable");
    amxc_var_delete(&external_config);
    amxc_var_new(&external_config);
    amxc_var_copy(external_config, args);

    amxp_timer_stop(restart_timer);
    amxp_timer_stop(reload_timer);

    if(enabled) {
        if(amxp_subproc_is_running(miniupnpd_proc)) {
            SAH_TRACEZ_NOTICE(ME, "Restarting miniupnpd");
            stop_miniupnpd();
        } else {
            SAH_TRACEZ_NOTICE(ME, "Starting miniupnpd");
            start_netmodel_callback();
        }
        start_miniupnpd();
    } else {
        SAH_TRACEZ_NOTICE(ME, "Shutting down miniupnpd");
        stop_miniupnpd();
        stop_netmodel_callback();
    }
    return 0;
}

static int init_config(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    if(plugin_config) {
        return 0;
    }
    amxc_var_new(&plugin_config);
    amxc_var_copy(plugin_config, args);

    return 0;
}

static int get_capabilities(UNUSED const char* function_name,
                            UNUSED amxc_var_t* args,
                            amxc_var_t* ret) {
    amxc_var_add_key(uint32_t, ret, "UPnPIGD", 2);

    return 0;
}

static void restart_miniupnpd_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    amxp_timer_stop(reload_timer);
    if(amxp_subproc_is_running(miniupnpd_proc)) {
        stop_miniupnpd();
    }
    start_miniupnpd();
}

static void reload_miniupnpd_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    when_true(amxp_timer_get_state(restart_timer) == amxp_timer_running, exit);
    if(amxp_subproc_is_running(miniupnpd_proc)) {
        kill(miniupnpd_proc->pid, SIGUSR1);
        SAH_TRACEZ_INFO(ME, "Reload config");
    } else {
        start_miniupnpd();
    }
exit:
    return;
}

static AMXM_CONSTRUCTOR module_init(void) {
    amxm_shared_object_t* so = NULL;
    int rv = -1;
    FILE* fp;
    char version[128];
    char* vers_num = NULL;

    so = amxm_so_get_current();
    when_null(so, exit);

    rv = amxm_module_register(&mod, so, "upnp");
    when_false(0 == rv, exit);

    rv = amxm_module_add_function(mod, "igd-changed", igd_changed);
    rv += amxm_module_add_function(mod, "init", init_config);
    rv += amxm_module_add_function(mod, "get_capabilities", get_capabilities);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add functions to module");
    }

    amxp_timer_new(&restart_timer, restart_miniupnpd_cb, NULL);
    amxp_timer_new(&reload_timer, reload_miniupnpd_cb, NULL);

    fp = popen("miniupnpd --version", "r");
    when_null_trace(fp, exit, ERROR, "Failed to get miniupnpd version");
    if(fgets(version, sizeof(version), fp) != NULL) {
        vers_num = strstr(version, MINIUPNP_CONF_VERSION);
        alt_com = vers_num != NULL;
    }
    pclose(fp);

exit:
    return rv;
}

static AMXM_DESTRUCTOR module_exit(void) {
    stop_miniupnpd();
    free(ext_ifname);
    free(ext_ifname6);
    free(miniupnpd_uuid);
    free(listening_ip);
    amxc_var_delete(&plugin_config);
    amxc_var_delete(&external_config);
    amxp_timer_delete(&restart_timer);
    amxp_timer_delete(&reload_timer);
    return 0;
}
