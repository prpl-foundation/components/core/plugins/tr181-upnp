include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C mod-upnp-uci/src all
	$(MAKE) -C mod-upnp-miniupnp/src all
ifeq ($(CONFIG_SAH_AMX_TR181_UPNP_MOD_UPNPADVERTISE_LIBUPNP),y)
	$(MAKE) -C mod-upnpadvertise-libupnp/src all
endif
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean
	$(MAKE) -C doc clean
	$(MAKE) -C mod-upnp-uci/src clean
	$(MAKE) -C mod-upnp-miniupnp/src clean
	$(MAKE) -C mod-upnpadvertise-libupnp/src clean
	$(MAKE) -C odl clean

install: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-upnp-uci.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-upnp-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-upnp-miniupnp.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-upnp-miniupnp.so
ifeq ($(CONFIG_SAH_AMX_TR181_UPNP_MOD_UPNPADVERTISE_LIBUPNP),y)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-upnpadvertise-libupnp.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-upnpadvertise-libupnp.so
endif
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vendorcfg.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_vendorcfg.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.d
	$(foreach odl,$(wildcard odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.d/;)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-upnp_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 acl/$(COMPONENT).json $(DEST)/usr/share/acl.d/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-upnp-uci.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-upnp-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-upnp-miniupnp.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-upnp-miniupnp.so
ifeq ($(CONFIG_SAH_AMX_TR181_UPNP_MOD_UPNPADVERTISE_LIBUPNP),y)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-upnpadvertise-libupnp.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-upnpadvertise-libupnp.so
endif
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vendorcfg.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_vendorcfg.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-upnp_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 acl/$(COMPONENT).json $(PKGDIR)/usr/share/acl.d/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc

	$(eval ODLFILES += odl/$(COMPONENT)_definition.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test