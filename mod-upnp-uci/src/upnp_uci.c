/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <debug/sahtrace_macros.h>

#include "upnp_uci.h"

#define ME "upnp"
#define string_empty(X) ((X == NULL) || (*X == '\0'))

static amxm_module_t* mod = NULL;

static amxb_bus_ctx_t* uci_bus(void) {
    static amxb_bus_ctx_t* bus = NULL;

    if(bus == NULL) {
        bus = amxb_be_who_has("uci.");
    }

    return bus;
}

static bool uci_commit(void) {
    amxb_bus_ctx_t* bus = uci_bus();
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(bus, exit, ERROR, "Bus not found that serves 'uci.'");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "config", "upnpd");

    rv = amxb_call(bus, "uci.", "commit", &args, &ret, 5);
    if(rv != AMXB_STATUS_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not call 'uci commit', reason %d", rv);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "UCI commit");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv == AMXB_STATUS_OK;
}

static bool ubus_call_uci(const char* method, amxc_var_t* args) {
    amxb_bus_ctx_t* bus = uci_bus();
    amxc_var_t ret;
    int rv = AMXB_ERROR_UNKNOWN;

    amxc_var_init(&ret);

    when_null_trace(bus, exit, ERROR, "Bus not found that serves 'uci.'");

    rv = amxb_call(bus, "uci.", method, args, &ret, 5);
    if(rv != AMXB_STATUS_OK) {
        SAH_TRACEZ_ERROR(ME, "Failed to call uci[%s], upnpd might not be reconfigured, reason %d", method, rv);
        goto exit;
    }

exit:
    amxc_var_clean(&ret);
    return rv == AMXB_STATUS_OK;
}

static bool upnp_args_init(amxc_var_t* args) {
    amxc_var_init(args);

    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, "config", "upnpd");
    amxc_var_add_key(cstring_t, args, "type", "upnpd");

    return true;
}

static int get_capabilities(UNUSED const char* function_name,
                            UNUSED amxc_var_t* args,
                            amxc_var_t* ret) {
    amxc_var_add_key(uint32_t, ret, "UPnPIGD", 2);

    return 0;
}

static int igd_changed(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    amxc_var_t* values = NULL;
    amxc_var_t upnp_args;

    upnp_args_init(&upnp_args);

    values = amxc_var_add_key(amxc_htable_t, &upnp_args, "values", NULL);
    amxc_var_add_key(bool, values, "enabled", GET_BOOL(args, "enabled"));

    ubus_call_uci("set", &upnp_args);
    uci_commit(); // procd will trigger /etc/init.d/miniupnpd reload

    amxc_var_clean(&upnp_args);
    return 0;
}

static AMXM_CONSTRUCTOR module_init(void) {
    amxm_shared_object_t* so = NULL;
    int rv = -1;

    so = amxm_so_get_current();
    when_null(so, exit);

    rv = amxm_module_register(&mod, so, "upnp");
    when_false(0 == rv, exit);

    rv = amxm_module_add_function(mod, "igd-changed", igd_changed);
    rv += amxm_module_add_function(mod, "get_capabilities", get_capabilities);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add functions to module");
    }

exit:
    return rv;
}

static AMXM_DESTRUCTOR module_exit(void) {
    amxm_module_deregister(&mod);
    return 0;
}
