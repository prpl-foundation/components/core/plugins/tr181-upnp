/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE

#include <upnp/upnp.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <netmodel/client.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "upnp_advertise.h"

#define ME "advert-libupnp"

static amxm_module_t* mod = NULL;
static amxc_var_t root_devices;
static bool advertising = false;
static bool start_requested = false;
static amxc_var_t* plugin_config = NULL;
static amxc_string_t intf;
static amxp_timer_t* restart_timer;
static uint32_t restart_time = BASE_RESTART_TIME;
static netmodel_query_t* lan_addr_query = NULL;

//GCOV_EXCL_START trampoline function not to be covered
static int _add_rootdevice(UNUSED const char* sig_name, amxc_var_t* args, amxc_var_t* ret) {
    int rv = -1;

    rv = add_rootdevice(args);
    amxc_var_set_bool(ret, rv == 0);
    return rv;
}

static int _del_rootdevice(UNUSED const char* sig_name, amxc_var_t* args, amxc_var_t* ret) {
    int rv = -1;

    rv = del_rootdevice(args);
    amxc_var_set_bool(ret, rv == 0);
    return rv;
}

static void lan_addr_changed(UNUSED const char* sig_name, const amxc_var_t* data, UNUSED void* priv) {
    set_advertise_intf_name(GETP_CHAR(data, "0.NetDevName"));
    if(start_requested || advertising) {
        start_advertising();
    }
}

static int _init_config(UNUSED const char* sig_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    init_config(args);
    return 0;
}

static int upnp_rootdevice_event(UNUSED Upnp_EventType EventType, UNUSED const void* Event, UNUSED void* Cookie) {
    SAH_TRACEZ_WARNING(ME, "Event rootdevice %d isn't supported", EventType);
    return 0;
}

static void restart_advertising(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    start_advertising();
}

static int _start_advertising(UNUSED const char* sig_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rv = 0;

    rv = start_advertising();
    amxc_var_set_bool(ret, rv == 0);
    return rv;
}

static int _stop_advertising(UNUSED const char* sig_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rv = 0;

    rv = stop_advertising();
    amxc_var_set_bool(ret, rv == 0);
    return rv;
}

static void create_upnp_firewall_rule(const char* name, const char* proto, uint32_t port, const char* iface, int ip_ver) {
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", name);
    amxc_var_add_key(cstring_t, &args, "protocol", proto);
    amxc_var_add_key(cstring_t, &args, "interface", iface);
    amxc_var_add_key(uint32_t, &args, "destination_port", port);
    amxc_var_add_key(uint32_t, &args, "ipversion", ip_ver);
    amxc_var_add_key(bool, &args, "enable", true);
    amxm_execute_function("fw", "fw", "set_service", &args, &ret);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void destroy_upnp_firewall_rule(const char* id) {
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);
    amxm_execute_function("fw", "fw", "delete_service", &args, &ret);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static AMXM_CONSTRUCTOR _module_init(void) {
    amxm_shared_object_t* so = NULL;
    int rv = -1;

    so = amxm_so_get_current();
    when_null(so, exit);

    rv = amxm_module_register(&mod, so, "upnpadvertise");
    when_false(0 == rv, exit);

    rv = amxm_module_add_function(mod, "start_advertising", _start_advertising);
    rv += amxm_module_add_function(mod, "stop_advertising", _stop_advertising);
    rv += amxm_module_add_function(mod, "add_rootdevice", _add_rootdevice);
    rv += amxm_module_add_function(mod, "del_rootdevice", _del_rootdevice);
    rv += amxm_module_add_function(mod, "init", _init_config);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add functions to module");
    }
    module_init();
exit:
    return rv;
}

static AMXM_DESTRUCTOR _module_exit(void) {
    module_exit();
    return 0;
}
//GCOV_EXCL_STOP

void set_advertise_intf_name(const char* name) {
    amxc_string_setf(&intf, "%s", name);
}

static void generate_miniupnpd_uuid(char* uuid) {
    int fd = -1;
    int retval;
    char buf[33] = {0};

    fd = open("/proc/sys/kernel/random/uuid", O_RDONLY);
    when_true(fd == -1, exit);
    retval = read(fd, buf, 32);
    if(retval == 32) {
        snprintf(uuid, 64, "uuid:%s", buf);
    }
    close(fd);
exit:
    return;
}

int amxc_var_to_xml_string(amxc_var_t* var, amxc_string_t* output) {
    int rv = -1;

    when_false(amxc_var_type_of(var) == AMXC_VAR_ID_HTABLE, exit);
    amxc_var_for_each(value, var) {
        if(amxc_var_type_of(value) == AMXC_VAR_ID_HTABLE) {
            amxc_string_appendf(output, "<%s>", amxc_var_key(value));
            amxc_var_to_xml_string(value, output);
            amxc_string_appendf(output, "</%s>", amxc_var_key(value));
        } else if(amxc_var_type_of(value) == AMXC_VAR_ID_LIST) {
            amxc_var_for_each(elem, value) {
                amxc_string_appendf(output, "<%s>", amxc_var_key(value));
                amxc_var_to_xml_string(elem, output);
                if(strcmp(amxc_var_key(value), "device") == 0) {
                    char uuid[64] = {0};

                    generate_miniupnpd_uuid(uuid);
                    amxc_string_appendf(output, "<UDN>%s</UDN>", uuid);
                }
                amxc_string_appendf(output, "</%s>", amxc_var_key(value));
            }
        } else {
            amxc_string_appendf(output, "<%s>", amxc_var_key(value));
            amxc_string_appendf(output, "%s", amxc_var_constcast(cstring_t, value));
            amxc_string_appendf(output, "</%s>", amxc_var_key(value));
        }
    }
    rv = 0;
exit:
    return rv;
}

int start_advertising(void) {
    int rv = -1;
    UpnpDevice_Handle upnp_hnd = -1;

    when_true_status(amxc_string_text_length(&intf) == 0, exit, start_requested = true);
    start_requested = false;
    if(advertising) {
        stop_advertising();
    }
    when_null(GETI_ARG(&root_devices, 0), exit);
    rv = UpnpInit2(amxc_string_get(&intf, 0), 0);
    when_failed(rv, check_error);
    create_upnp_firewall_rule(UPNPADVERT_FIREWALL_LISTEN_RULE, "TCP", UpnpGetServerPort(), GET_CHAR(plugin_config, "ip_intf_lan"), 4);
    amxc_var_for_each(root_device, &root_devices) {
        amxc_string_t buf_xml;
        uint32_t config_baseURL = 1;
        char uuid[64] = {0};

        amxc_string_init(&buf_xml, 0);
        generate_miniupnpd_uuid(uuid);
        amxc_var_add_key(cstring_t, root_device, "UDN", uuid);
        amxc_string_set(&buf_xml, "<?xml version=\"1.0\"?><root xmlns=\"urn:schemas-upnp-org:device-1-0\" configId=\"1337\"><specVersion><major>1</major><minor>1</minor></specVersion><device>");
        amxc_var_to_xml_string(root_device, &buf_xml);
        amxc_string_appendf(&buf_xml, "%s", "<presentationURL>");
        config_baseURL = amxc_string_text_length(&buf_xml);
        amxc_string_appendf(&buf_xml, "http://%s:%d/%s.xml</presentationURL>", UpnpGetServerIpAddress(), UpnpGetServerPort(), amxc_var_key(root_device));
        amxc_string_appendf(&buf_xml, "%s", "</device></root>");
        rv = UpnpRegisterRootDevice2(UPNPREG_BUF_DESC, amxc_string_get(&buf_xml, 0), amxc_string_text_length(&buf_xml), config_baseURL, upnp_rootdevice_event, amxc_var_key(root_device), &upnp_hnd);
        if(rv != UPNP_E_SUCCESS) {
            SAH_TRACEZ_ERROR(ME, "Couldn't register rootdevice %s in libupnp (%d)", amxc_var_key(root_device), rv);
            continue;
        }
        rv = UpnpSendAdvertisement(upnp_hnd, 100);
        if(rv != UPNP_E_SUCCESS) {
            SAH_TRACEZ_ERROR(ME, "Couldn't advertise rootdevice %s (%d)", amxc_var_key(root_device), rv);
        }
        amxc_string_clean(&buf_xml);
    }
    advertising = true;
    rv = 0;
check_error:
    if((rv <= -200) && (rv >= -299)) { //network issues
        amxp_timer_start(restart_timer, restart_time);
        SAH_TRACEZ_ERROR(ME, "Networking error while initializing libupnp: retry in %dsec(s)...", restart_time / 1000);
        restart_time = ((restart_time >= MAX_RESTART_TIME) ? (MAX_RESTART_TIME) : (restart_time * 2));
    } else {
        restart_time = BASE_RESTART_TIME;
    }
exit:
    return rv;
}


int stop_advertising(void) {
    int rv = 0;

    rv = UpnpFinish();
    destroy_upnp_firewall_rule(UPNPADVERT_FIREWALL_LISTEN_RULE);
    advertising = false;
    start_requested = false;
    return rv;
}

int add_rootdevice(amxc_var_t* args) {
    int rv = -1;
    const char* id = NULL;

    when_null(args, exit);
    id = GET_CHAR(args, "id");
    when_str_empty_trace(id, exit, ERROR, "argument is missing id label");
    when_null_trace(GET_ARG(args, "rootdevice"), exit, ERROR, "argument is missing rootdevice label");
    if(GET_ARG(&root_devices, id) != NULL) {
        amxc_var_t* old_data = NULL;

        old_data = amxc_var_take_key(&root_devices, id);
        amxc_var_delete(&old_data);
    }
    amxc_var_add_key(amxc_htable_t, &root_devices, id, amxc_var_constcast(amxc_htable_t, GET_ARG(args, "rootdevice")));
    rv = 0;
exit:
    return rv;
}

int del_rootdevice(amxc_var_t* args) {
    int rv = -1;
    const char* id = NULL;

    when_null(args, exit);
    id = GET_CHAR(args, "id");
    when_str_empty(id, exit);
    if(GET_ARG(&root_devices, id) != NULL) {
        amxc_var_t* old_data = NULL;

        old_data = amxc_var_take_key(&root_devices, id);
        amxc_var_delete(&old_data);
    }
    rv = 0;
exit:
    return rv;
}

void init_config(amxc_var_t* config) {
    when_not_null(plugin_config, exit);
    when_null(config, exit);

    amxc_var_new(&plugin_config);
    amxc_var_copy(plugin_config, config);
    lan_addr_query = netmodel_openQuery_getAddrs(GET_CHAR(plugin_config, "ip_intf_lan"), "mod-upnpadvertise-libupnp", "", "down", lan_addr_changed, NULL);
exit:
    return;
}

void module_init(void) {
    amxc_var_init(&root_devices);
    amxc_string_init(&intf, 0);
    amxc_var_set_type(&root_devices, AMXC_VAR_ID_HTABLE);
    amxp_timer_new(&restart_timer, restart_advertising, NULL);
}

void module_exit(void) {
    if(advertising) {
        stop_advertising();
    }
    if(lan_addr_query != NULL) {
        netmodel_closeQuery(lan_addr_query);
        lan_addr_query = NULL;
    }
    amxc_var_clean(&root_devices);
    amxc_var_delete(&plugin_config);
    amxp_timer_delete(&restart_timer);
    amxc_string_clean(&intf);
}
