/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include "upnp_advertise.h"
#include "test_upnp_advertise_libupnp.h"

static amxo_parser_t* parser = NULL;
static amxd_dm_t* dm = NULL;

int test_setup(UNUSED void** state) {
    const char* odl = "%config { ip_intf_lan = \"Device.IP.Interface.3.\"; } include \"../examples/tr181-upnp_advertised_devices.odl\";";
    amxd_object_t* root = NULL;
    int retval = 0;

    assert_int_equal(amxd_dm_new(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_new(&parser), 0);

    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    retval = amxo_parser_parse_string(parser, odl, root);
    assert_int_equal(retval, 0);
    module_init();
    return 0;
}

int test_teardown(UNUSED void** state) {
    module_exit();
    amxo_parser_delete(&parser);
    amxd_dm_delete(&dm);
    return 0;
}

void test_mod_init(UNUSED void** state) {
    assert_non_null(amxo_parser_get_config(parser, "."));
    init_config(amxo_parser_get_config(parser, "."));
    set_advertise_intf_name("eth0");
}

void test_amxc_var_to_xml_string(UNUSED void** state) {
    amxc_var_t var;
    amxc_string_t output;

    amxc_var_init(&var);
    amxc_string_init(&output, 0);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_to_xml_string(&var, &output);
    assert_string_equal(amxc_string_get(&output, 0), "");

    amxc_var_add_key(cstring_t, &var, "toto", "tata");
    amxc_var_to_xml_string(&var, &output);
    assert_string_equal(amxc_string_get(&output, 0), "<toto>tata</toto>");

    amxc_string_setf(&output, "%s", "");
    amxc_var_to_xml_string(amxo_parser_get_config(parser, "advertised_devices.extender"), &output);
    assert_non_null(strstr(amxc_string_get(&output, 0), "<deviceType>urn:schemas-upnp-org:device:InternetGatewayDevice:2</deviceType>"));

    amxc_var_clean(&var);
    amxc_string_clean(&output);
}

void test_rootdevice_management(UNUSED void** state) {
    amxc_var_t var;

    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    add_rootdevice(NULL);
    add_rootdevice(&var);
    amxc_var_add_key(amxc_htable_t, &var, "rootdevice", amxc_var_constcast(amxc_htable_t, amxo_parser_get_config(parser, "advertised_devices.extender")));
    add_rootdevice(&var);
    amxc_var_add_key(cstring_t, &var, "id", "extender");
    add_rootdevice(&var);
    add_rootdevice(&var);
    start_advertising();
    stop_advertising();
    del_rootdevice(&var);
    amxc_var_clean(&var);
}