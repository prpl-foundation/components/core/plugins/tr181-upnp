# Makefile variables
MACHINE = $(shell $(CC) -dumpmachine)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
SRCDIR = $(realpath ../../mod-upnpadvertise-libupnp/src)
INCDIRS = $(realpath ./include/ ../include_priv)
COVERREPORT = report
SOURCES += $(wildcard $(SRCDIR)/*.c) $(wildcard ./src/*.c)
OBJECTS = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES:.c=.o)))
TARGET = run_test

# compilation and linking flags
CFLAGS += -Wall -Wextra \
		  -Wformat=2 -Wshadow \
		  -Wwrite-strings -Wredundant-decls \
		  -Wmissing-declarations -Wno-attributes \
		  -Wno-format-nonliteral $(shell pkg-config --cflags cmocka)\
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
		  -fPIC -g3 $(addprefix -I ,$(INCDIRS))

LDFLAGS += -fPIC $(STAGING_LIBDIR) $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxm -lamxp -lamxd -lamxo -lsahtrace -lnetmodel -lupnp

# Makefile rules
run: $(OBJDIR)/$(TARGET)
	set -o pipefail; valgrind --leak-check=full --show-leak-kinds=all --keep-debuginfo=yes --exit-on-first-error=yes --error-exitcode=1 $< 2>&1 | tee -a $(OBJDIR)/unit_test_results.txt;

clean:
	rm -rf $(OBJDIR)
	rm -rf $(OBJDIR)/$(COVERREPORT)
	find .. -name "run_test*" -delete

-include $(OBJECTS:.o=.d)

$(OBJDIR)/:
	mkdir -p $@

$(OBJDIR)/$(COVERREPORT)/:
	mkdir -p $@

# modules sources
$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

# test sources
$(OBJDIR)/%.o: ./src/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

# test binary
$(OBJDIR)/$(TARGET): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS) -fprofile-arcs -ftest-coverage

.PHONY: run clean coverage $(TARGET)
