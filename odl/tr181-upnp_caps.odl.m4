%config {
    %global privileges = {
        user = "USER_ID", group = "GROUP_ID", capabilities = ["CAP_NET_ADMIN", "CAP_NET_RAW", "CAP_KILL" , "CAP_DAC_OVERRIDE", "CAP_SETFCAP"]
    };
}