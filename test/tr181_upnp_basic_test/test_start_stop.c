/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "dm_device.h"
#include "tr181-upnp.h"
#include "test_start_stop.h"
#include "mock.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

int test_setup(UNUSED void** state) {
    amxd_object_t* root = NULL;
    int retval = 0;
    const char* odl =
        "%config {"
        "    prefix_ = \"X_PRPL-COM_\";"
        "    include-dirs = ["
        "        \"../../odl/\""
        "    ];"
        "    modules = {"
        "        upnp-directory = \"" MOD_DIR "\","
        "        fw-directory = \"../common/modules/\""
        "    };"
        "}"
        "include \"tr181-upnp_definition.odl\";"
        "include \"../mod-upnpadvertise-libupnp/examples/tr181-upnp_advertised_devices.odl\";"
        "%define {"
        "}";

    assert_int_equal(amxd_dm_new(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_new(&parser), 0);

    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    //events
    amxo_resolver_ftab_add(parser, "app_start", AMXO_FUNC(_app_start));
    amxo_resolver_ftab_add(parser, "igd_changed", AMXO_FUNC(igd_changed));

    test_init_dummy_dm(dm, parser);

    retval = amxo_parser_parse_string(parser, odl, root);
    printf("PARSER MESSAGE = %s\n", amxc_string_get(&parser->msg, 0));
    assert_int_equal(retval, 0);

    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:stop", NULL);
    handle_events();

    test_clean_dummy_dm(dm, parser);

    amxo_parser_delete(&parser);
    amxd_dm_delete(&dm);

    return 0;
}

void test_can_start_plugin(UNUSED void** state) {
    assert_int_equal(_main(0, dm, parser), 0);

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);
    handle_events();
}

void test_can_stop_plugin(UNUSED void** state) {
    assert_int_equal(_main(1, dm, parser), 0);
}

void test_igd_changed(UNUSED void** state) {
    igd_changed();
}
