/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>

#include <amxb/amxb.h>

#include "mock.h"
#include "tr181-upnp.h"

typedef struct {
    amxc_htable_it_t it;
    char* subscriber;
    char* method;
    char* path;
} dummy_query_t;

static amxd_dm_t* dummy_dm = NULL;
static amxo_parser_t* dummy_parser = NULL;
static amxc_htable_t* query_table = NULL;

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_status_t _set(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static amxd_status_t _delete(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             UNUSED amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static amxd_status_t _commit(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             UNUSED amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static amxd_status_t _getResult(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxc_htable_it_t* it = NULL;
    dummy_query_t* q = NULL;
    char* path = NULL;
    char* end_of_path = NULL;

    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    end_of_path = strstr(path, ".Query");
    if(end_of_path != NULL) {
        *end_of_path = '\0';
    }

    it = amxc_htable_get(query_table, path);
    q = amxc_htable_it_get_data(it, dummy_query_t, it);
    assert_non_null(q);

    if(strcmp(q->method, "getFirstParameter") == 0) {
        amxc_var_set(cstring_t, ret, "eth0");
    }

    free(path);

    return amxd_status_ok;
}

static amxd_status_t _openQuery(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    char* path = NULL;
    dummy_query_t* q = NULL;

    if(query_table == NULL) {
        amxc_htable_new(&query_table, 5);
    }

    q = calloc(1, sizeof(*q));
    q->subscriber = strdup(GETP_CHAR(args, "subscriber"));
    q->method = strdup(GETP_CHAR(args, "class"));
    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    amxc_htable_insert(query_table, path, &q->it);

    amxc_var_set(uint32_t, ret, 1);
    free(path);
    return amxd_status_ok;
}

static amxd_status_t _closeQuery(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    char* path = NULL;
    amxc_htable_it_t* it = NULL;
    dummy_query_t* q = NULL;

    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    it = amxc_htable_take(query_table, path);
    q = amxc_htable_it_get_data(it, dummy_query_t, it);
    assert_non_null(q);

    amxc_htable_it_clean(&q->it, NULL);
    free(q->subscriber);
    free(q->path);
    free(q->method);
    free(q);

    if(amxc_htable_is_empty(query_table)) {
        amxc_htable_delete(&query_table, NULL);
    }

    free(path);

    return amxd_status_ok;
}

static void test_init_dummy_fn_resolvers(amxo_parser_t* parser) {
    // uci
    amxo_resolver_ftab_add(parser, "set", AMXO_FUNC(_set));
    amxo_resolver_ftab_add(parser, "delete", AMXO_FUNC(_delete));
    amxo_resolver_ftab_add(parser, "commit", AMXO_FUNC(_commit));

    // NetModel
    amxo_resolver_ftab_add(parser, "getResult", AMXO_FUNC(_getResult));
    amxo_resolver_ftab_add(parser, "openQuery", AMXO_FUNC(_openQuery));
    amxo_resolver_ftab_add(parser, "closeQuery", AMXO_FUNC(_closeQuery));
}

void test_init_dummy_dm(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_object_t* root = NULL;

    dummy_dm = dm;
    dummy_parser = parser;

    test_register_dummy_be();
    test_init_dummy_fn_resolvers(parser);

    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    assert_int_equal(amxo_parser_parse_file(parser, "../common/mock_uci.odl", root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, "../common/mock_netmodel.odl", root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, "../common/mock_ip.odl", root), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(amxo_connection_add(parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx), 0);
    // Register data model
    assert_int_equal(amxb_register(bus_ctx, dm), 0);
}

void test_clean_dummy_dm(UNUSED amxd_dm_t* dm, UNUSED amxo_parser_t* parser) {
    test_unregister_dummy_be();
    dummy_dm = NULL;
    dummy_parser = NULL;
}
