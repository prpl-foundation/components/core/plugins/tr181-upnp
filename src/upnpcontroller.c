/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-upnp.h"

#include <amxm/amxm.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>

#define UPNP_FIREWALL_LISTEN_RULE "tr181-upnp-listen"
#define UPNP_FIREWALL_SSDP_RULE "tr181-upnp-ssdp"
#define ME "upnp"

static void create_upnp_firewall_rule(const char* name, const char* proto, uint32_t port, const char* iface, int ip_ver) {
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", name);
    amxc_var_add_key(cstring_t, &args, "protocol", proto);
    amxc_var_add_key(cstring_t, &args, "interface", iface);
    amxc_var_add_key(uint32_t, &args, "destination_port", port);
    amxc_var_add_key(uint32_t, &args, "ipversion", ip_ver);
    amxc_var_add_key(bool, &args, "enable", true);
    amxm_execute_function("fw", "fw", "set_service", &args, &ret);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void create_upnp_pmp_firewall_rules(void) {
    const char* iface = GET_CHAR(get_config(), "ip_intf_lan");

    create_upnp_firewall_rule(UPNP_FIREWALL_LISTEN_RULE, "6", 5000, iface, 4);
    create_upnp_firewall_rule(UPNP_FIREWALL_LISTEN_RULE "6", "6", 5000, iface, 6);
}

static void create_upnp_ssdp_firewall_rules(void) {
    const char* iface = GET_CHAR(get_config(), "ip_intf_lan");

    create_upnp_firewall_rule(UPNP_FIREWALL_SSDP_RULE, "17", 1900, iface, 4);
    create_upnp_firewall_rule(UPNP_FIREWALL_SSDP_RULE "6", "17", 1900, iface, 6);
}


static void destroy_upnp_firewall_rule(const char* id) {
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", id);
    amxm_execute_function("fw", "fw", "delete_service", &args, &ret);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void destroy_upnp_pmp_firewall_rules(void) {
    destroy_upnp_firewall_rule(UPNP_FIREWALL_LISTEN_RULE);
    destroy_upnp_firewall_rule(UPNP_FIREWALL_LISTEN_RULE "6");
}

void destroy_upnp_ssdp_firewall_rules(void) {
    destroy_upnp_firewall_rule(UPNP_FIREWALL_SSDP_RULE);
    destroy_upnp_firewall_rule(UPNP_FIREWALL_SSDP_RULE "6");
}

void init_upnp_module(void) {
    amxc_var_t ret;

    amxc_var_init(&ret);
    if(0 != amxm_execute_function("upnp", "upnp", "init", get_config(), &ret)) {
        SAH_TRACEZ_WARNING(ME, "Module upnp failed to execute function init");
    }
    if(upnpadvertise && (0 != amxm_execute_function("upnpadvertise", "upnpadvertise", "init", get_config(), &ret))) {
        SAH_TRACEZ_WARNING(ME, "Module upnpadvertise failed to execute function init");
    }
    amxc_var_clean(&ret);
}

void get_capabilities_upnp_module(void) {
    amxc_var_t ret;
    amxd_object_t* obj_capabilities = NULL;
    amxd_trans_t transaction;
    amxd_status_t status = amxd_status_ok;

    amxc_var_init(&ret);
    amxc_var_set_type(&ret, AMXC_VAR_ID_HTABLE);
    if(0 != amxm_execute_function("upnp", "upnp", "get_capabilities", get_config(), &ret)) {
        SAH_TRACEZ_WARNING(ME, "Module failed to execute function get_capabilities");
    }

    obj_capabilities = amxd_dm_findf(get_dm(), "UPnP.Device.Capabilities.");

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, obj_capabilities);

    amxc_var_for_each(value, &ret) {
        const char* param_name = amxc_var_key(value);
        amxd_trans_set_value(uint32_t, &transaction, param_name, GET_UINT32(&ret, param_name));
    }

    status = amxd_trans_apply(&transaction, get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_WARNING(ME, "UPnP.Device.Capabilities set failed (%d)", status);
    }

    amxd_trans_clean(&transaction);

    amxc_var_clean(&ret);
}

static int get_device_info(amxc_var_t* device_info) {
    int rv = -1;

    when_true_status(amxc_var_type_of(device_info) != AMXC_VAR_ID_NULL, exit, rv = 0);
    rv = amxb_get(amxb_be_who_has("DeviceInfo."), "DeviceInfo.", 0, device_info, 1);
    when_failed_trace(rv, exit, WARNING, "Couldn't get DeviceInfo. (%d)", rv);

exit:
    return rv;
}

static void add_vendorcfg_param(amxc_var_t* config, amxc_var_t* device_info, const char* param_name, const char* device_info_var_path) {
    if(string_empty(GET_CHAR(get_config(), param_name)) == false) {
        amxc_var_add_key(cstring_t, config, param_name, GET_CHAR(get_config(), param_name));
    } else {
        when_failed(get_device_info(device_info), exit);
        if(string_empty(GETP_CHAR(device_info, device_info_var_path)) == false) {
            amxc_var_add_key(cstring_t, config, param_name, GETP_CHAR(device_info, device_info_var_path));
        }
    }
exit:
    return;
}

static void get_igd_config(amxc_var_t* args) {
    amxd_object_t* obj_device = NULL;
    amxd_object_t* obj_config = NULL;
    amxc_var_t device_info;
    amxc_var_t params;

    amxc_var_init(&params);
    amxc_var_init(&device_info);

    obj_device = amxd_dm_findf(get_dm(), "UPnP.Device.");
    obj_config = amxd_dm_findf(get_dm(), "UPnP.%sIGDConfig.", GET_CHAR(get_config(), "prefix_"));
    when_null_trace(obj_device, exit, ERROR, "Could not get UPnP.Device object");
    when_null_trace(obj_config, exit, ERROR, "Could not get UPnP.%sIGDConfig object", GET_CHAR(get_config(), "prefix_"));
    when_null_trace(args, exit, ERROR, "args is NULL, can't use it");

    amxd_object_get_params(obj_device, &params, amxd_dm_access_private);
    amxc_var_add_new_key_bool(args, "enable", GET_BOOL(&params, "Enable") ? GET_BOOL(&params, "UPnPIGD") : false);
    amxc_var_clean(&params);

    amxc_var_init(&params);
    amxd_object_get_params(obj_config, &params, amxd_dm_access_private);
    amxc_var_set_key(args, "ext_stun_host", GET_ARG(&params, "STUNHostURL"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(args, "auto_cleanup_enable", GET_ARG(&params, "AutoCleanupEnable"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(args, "max_lifetime", GET_ARG(&params, "MaxLifetime"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(args, "clean_ruleset_interval", GET_ARG(&params, "CleanupInterval"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(args, "enable_natpmp", GET_ARG(&params, "NATPMPPCPSupport"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(args, "allow_reserved_addr", GET_ARG(&params, "AllowReservedAddr"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(args, "upnp_amx_wan_interface", GET_ARG(&params, "UPnPInterface"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(args, "enable_ipv6", GET_ARG(&params, "EnableIPv6"), AMXC_VAR_FLAG_COPY);
    add_vendorcfg_param(args, &device_info, "friendly_name", "0.'DeviceInfo.'.FriendlyName");
    add_vendorcfg_param(args, &device_info, "manufacturer_name", "0.'DeviceInfo.'.Manufacturer");
    add_vendorcfg_param(args, &device_info, "manufacturer_url", "0.'DeviceInfo.'.X_PRPL-COM_ManufacturerURL");
    add_vendorcfg_param(args, &device_info, "model_name", "0.'DeviceInfo.'.ModelName");
    add_vendorcfg_param(args, &device_info, "model_description", "0.'DeviceInfo.'.X_PRPL-COM_ModelDescription");
    add_vendorcfg_param(args, &device_info, "model_url", "0.'DeviceInfo.'.X_PRPL-COM_ModelURL");
    add_vendorcfg_param(args, &device_info, "model_number", "0.'DeviceInfo.'.ModelNumber");
    add_vendorcfg_param(args, &device_info, "serial", "0.'DeviceInfo.'.SerialNumber");

exit:
    amxc_var_clean(&params);
    amxc_var_clean(&device_info);
    return;
}

void igd_changed(void) {
    amxc_var_t ret;
    amxc_var_t args;
    bool enable = false;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    get_igd_config(&args);
    enable = GET_BOOL(&args, "enable");

    if(enable) {
        create_upnp_pmp_firewall_rules();
        create_upnp_ssdp_firewall_rules();
    } else {
        destroy_upnp_pmp_firewall_rules();
        destroy_upnp_ssdp_firewall_rules();
        amxb_del(amxb_be_who_has("PCP."), "PCP.Client.*.Server.*.InboundMapping.[Origin == \"UPnP_IWF\"].", 0, NULL, NULL, 5);
    }

    if(0 != amxm_execute_function("upnp", "upnp", "igd-changed", &args, &ret)) {
        SAH_TRACEZ_ERROR(ME, "Module failed to execute function igd-changed");
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
