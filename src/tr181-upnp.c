/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <dlfcn.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/client.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_path.h>
#include <amxm/amxm.h>
#include "tr181-upnp.h"
#include "dm_device.h"

#define ME "upnp"

bool upnpadvertise;

typedef struct {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
} app_t;

static app_t app;

amxd_dm_t* get_dm(void) {
    return app.dm;
}

amxo_parser_t* get_parser(void) {
    return app.parser;
}

amxc_var_t* get_config(void) {
    amxo_parser_t* parser = get_parser();
    return parser != NULL ? &(parser->config) : NULL;
}

static bool load_module(const char* dir, const char* module, const char* controller) {
    const char* path = NULL;
    amxc_string_t path_str;
    amxm_shared_object_t* so = NULL;
    int rv = -1;

    amxc_string_init(&path_str, 0);

    when_str_empty(module, exit);
    when_str_empty(dir, exit);

    amxc_string_setf(&path_str, "%s/%s.so", dir, module);
    path = amxc_string_get(&path_str, 0);

    SAH_TRACEZ_NOTICE(ME, "Load module[%s] for %s", path, controller);

    rv = amxm_so_open(&so, controller, path);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load module[%s]: %s", path, dlerror());
        goto exit;
    }

exit:
    amxc_string_clean(&path_str);
    return rv == 0;
}

static bool load_upnp_module(amxd_object_t* obj) {
    const char* module = amxc_var_constcast(cstring_t, amxd_object_get_param_value(obj, "UPnPController"));
    const char* dir = GETP_CHAR(get_config(), "modules.upnp-directory");

    return load_module(dir, module, "upnp");
}

static bool load_firewall_module(amxd_object_t* obj) {
    const char* module = amxc_var_constcast(cstring_t, amxd_object_get_param_value(obj, "FWController"));
    const char* dir = GETP_CHAR(get_config(), "modules.fw-directory");

    return load_module(dir, module, "fw");
}

static bool load_upnpadvertise_module(amxd_object_t* obj) {
    const char* module = amxc_var_constcast(cstring_t, amxd_object_get_param_value(obj, "UPnPAdvertiseController"));
    const char* dir = GETP_CHAR(get_config(), "modules.upnp-directory");

    return load_module(dir, module, "upnpadvertise");
}

static bool load_modules(amxd_object_t* obj) {
    bool rv = true;

    rv &= load_upnp_module(obj);
    rv &= load_firewall_module(obj);
    upnpadvertise = load_upnpadvertise_module(obj);

    return rv;
}

static void unload_modules(void) {
    amxm_close_all();
}

static int init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int rv = -1;

    memset(&app, 0, sizeof(app));
    app.dm = dm;
    app.parser = parser;
    when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to initialize libnetmodel!");
    rv = 0;
exit:
    return rv;
}

static int cleanup(void) {
    destroy_upnp_pmp_firewall_rules();
    unload_modules();
    netmodel_cleanup();
    app.dm = NULL;
    app.parser = NULL;
    return 0;
}

static void resolve_dm_values(amxc_var_t* dm_resolved_values) {
    amxc_var_t* dm_values = GET_ARG(get_config(), "dm_values");

    when_null(dm_values, exit);
    when_null(dm_resolved_values, exit);
    amxc_var_set_type(dm_resolved_values, AMXC_VAR_ID_HTABLE);
    amxc_var_for_each(dm_value, dm_values) {
        amxd_status_t status;
        amxd_path_t path;
        amxc_var_t ret;

        status = amxd_path_init(&path, GET_CHAR(dm_value, NULL));
        if(status != amxd_status_ok) {
            amxd_path_clean(&path);
            SAH_TRACEZ_ERROR(ME, "Path is invalid: %s", GET_CHAR(dm_value, NULL));
            continue;
        }
        amxc_var_init(&ret);
        amxb_get(amxb_be_who_has(amxd_path_get(&path, AMXD_OBJECT_TERMINATE)), GET_CHAR(dm_value, NULL), 0, &ret, 1);
        amxc_var_add_key(cstring_t, dm_resolved_values, amxc_var_key(dm_value), GETP_CHAR(&ret, "0.0.0"));
        amxc_var_clean(&ret);
        amxd_path_clean(&path);
    }
exit:
    return;
}

static void resolve_rootdevice_values(amxc_var_t* rootdevice, amxc_var_t* resolved, amxc_var_t* dm_resolved_values) {

    amxc_var_for_each(var, rootdevice) {
        if(amxc_var_type_of(var) == AMXC_VAR_ID_CSTRING) {
            amxc_string_t value;
            amxc_var_t* new_key = NULL;

            amxc_string_init(&value, 0);
            amxc_string_setf(&value, "%s", GET_CHAR(var, NULL));
            amxc_string_resolve_var(&value, dm_resolved_values);
            new_key = amxc_var_add_new_key(resolved, amxc_var_key(var));
            amxc_var_set_type(new_key, AMXC_VAR_ID_CSTRING);
            amxc_var_push(cstring_t, new_key, amxc_string_take_buffer(&value));
            amxc_string_clean(&value);
        } else if(amxc_var_type_of(var) == AMXC_VAR_ID_HTABLE) {
            amxc_var_t* new_htable = NULL;

            new_htable = amxc_var_add_new_key(resolved, amxc_var_key(var));
            amxc_var_set_type(new_htable, AMXC_VAR_ID_HTABLE);
            resolve_rootdevice_values(var, new_htable, dm_resolved_values);
        } else if(amxc_var_type_of(var) == AMXC_VAR_ID_LIST) {
            amxc_var_t* new_list = NULL;

            new_list = amxc_var_add_new_key(resolved, amxc_var_key(var));
            amxc_var_set_type(new_list, AMXC_VAR_ID_LIST);
            amxc_var_for_each(elem, var) {
                if(amxc_var_type_of(elem) == AMXC_VAR_ID_HTABLE) {
                    amxc_var_t* new_htable = NULL;

                    new_htable = amxc_var_add_new(new_list);
                    amxc_var_set_type(new_htable, AMXC_VAR_ID_HTABLE);
                    resolve_rootdevice_values(elem, new_htable, dm_resolved_values);
                } else {
                    SAH_TRACEZ_WARNING(ME, "Ignoring list elem that isn't an htable");
                }
            }
        } else {
            SAH_TRACEZ_WARNING(ME, "Ignoring value that isn't cstring_t, htable, or list");
        }
    }
}

static void add_rootdevice_to_var(amxc_var_t* var, amxc_var_t* device, amxc_var_t* dm_resolved_values) {
    amxc_var_t rootdevice;
    amxc_var_t* tmp = NULL;

    amxc_var_init(&rootdevice);
    amxc_var_set_type(&rootdevice, AMXC_VAR_ID_HTABLE);
    resolve_rootdevice_values(device, &rootdevice, dm_resolved_values);
    tmp = amxc_var_add_new_key(var, "rootdevice");
    amxc_var_move(tmp, &rootdevice);
    amxc_var_clean(&rootdevice);
}

void advertise_device(void) {
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t dm_resolved_values;

    amxc_var_init(&args);
    amxc_var_init(&dm_resolved_values);
    when_false(upnpadvertise, exit);
    resolve_dm_values(&dm_resolved_values);
    when_null(GET_ARG(get_config(), "advertised_devices"), exit);
    amxc_var_for_each(device, GET_ARG(get_config(), "advertised_devices")) {
        amxc_var_init(&ret);
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "id", amxc_var_key(device));
        add_rootdevice_to_var(&args, device, &dm_resolved_values);
        amxm_execute_function("upnpadvertise", "upnpadvertise", "add_rootdevice", &args, &ret);
        if(!GET_BOOL(&ret, NULL)) {
            SAH_TRACEZ_ERROR(ME, "Failed to add rootdevice: %s", amxc_var_key(device));
        }
        amxc_var_clean(&ret);
    }
    amxc_var_init(&ret);
    amxm_execute_function("upnpadvertise", "upnpadvertise", "start_advertising", &args, &ret);
    amxc_var_clean(&ret);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&dm_resolved_values);
}

void _app_start(UNUSED const char* const sig_name,
                UNUSED const amxc_var_t* const data,
                UNUSED void* const priv) {
    SAH_TRACEZ_NOTICE(ME, "app-start");
    load_modules(amxd_dm_findf(get_dm(), "UPnP"));
    init_upnp_module();
    get_capabilities_upnp_module();
    igd_changed();
    advertise_device();
}

int _main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;

    switch(reason) {
    case AMXO_START:
        SAH_TRACEZ_NOTICE(ME, "entrypoint - start");
        retval = init(dm, parser);
        break;

    case AMXO_STOP:
        SAH_TRACEZ_NOTICE(ME, "entrypoint - stop");
        retval = cleanup();
        break;
    }

    return retval;
}
