# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.11.5 - 2025-01-03(15:41:13 +0000)

## Release v0.11.4 - 2024-11-28(11:27:50 +0000)

### Changes

- - [CDROUTER][UPnP] ConnectionStatus value in NOTIFY event is NOT correctly updated after WAN connectivity Loss

## Release v0.11.3 - 2024-11-28(10:49:20 +0000)

### Fixes

- - [UPnP] DUT Not Announced as UPnP Device on LAN Side After HGW IP Update

## Release v0.11.2 - 2024-11-19(10:55:40 +0000)

## Release v0.11.1 - 2024-11-09(11:40:50 +0000)

## Release v0.11.0 - 2024-11-08(11:57:05 +0000)

### New

- - Extender advertisement via UPnP/SSDP

## Release v0.10.18 - 2024-11-05(12:49:19 +0000)

### Other

- - [UPnP-IGD] PCP entries created by UPnP-IGD are not cleared when the UPnP-IGD is disabled

## Release v0.10.17 - 2024-10-02(18:45:51 +0000)

### Other

- - [tr181-upnp] make it possible to disable ipv6 on upnp

## Release v0.10.16 - 2024-09-19(06:41:24 +0000)

### Changes

- - [UPnP][DSLite] Miniupnpd is using developer's tool as feature

## Release v0.10.15 - 2024-09-10(07:10:37 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.10.14 - 2024-09-09(13:32:26 +0000)

### Other

- - [tr181-upnp] Add Device.UPnP.Device.Capabilities.

## Release v0.10.13 - 2024-09-09(10:18:46 +0000)

### Other

- - [UPnP] Closing tr181-upnp doesn't shutdown miniupnpd (fix)

## Release v0.10.12 - 2024-08-01(09:54:46 +0000)

### Other

- - [UPnP][DSLite] UPnP is not creating PCP instances in a DSLite environment

## Release v0.10.11 - 2024-07-23(07:58:18 +0000)

### Fixes

- Better shutdown script

## Release v0.10.10 - 2024-07-15(14:03:04 +0000)

### Other

- - [CDROUTER][UPNP][REGRESSION] IPv6 Pinholes are not effective. traffic not forwarded

## Release v0.10.9 - 2024-07-08(07:52:22 +0000)

## Release v0.10.8 - 2024-07-03(09:57:01 +0000)

### Fixes

- Do not write NULL bytes to /var/etc/miniupnpd.conf

## Release v0.10.7 - 2024-06-25(08:24:48 +0000)

### Other

- amx plugin should not run as root user

## Release v0.10.6 - 2024-06-24(12:21:51 +0000)

### Other

- - [Security][UPnP] Disable NAT-PMP

## Release v0.10.5 - 2024-06-18(11:00:08 +0000)

### Changes

- mod-upnp-miniupnpd: Reload configuration (HOP-3580)

### Other

- - [CDROUTER][UPnP] ConnectionStatus value in NOTIFY event is

## Release v0.10.4 - 2024-05-16(10:30:18 +0000)

### Other

- UPnP service missing under PersistentConfiguration.Service. param

## Release v0.10.3 - 2024-05-02(08:15:05 +0000)

### Other

- - [UPnP] Add global enable [add]

## Release v0.10.2 - 2024-04-10(07:12:19 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.10.1 - 2024-03-19(12:59:17 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.10.0 - 2024-02-05(16:20:30 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.9.0 - 2024-01-17(09:31:51 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.8.0 - 2024-01-11(14:39:07 +0000)

### New

- - [UPnP-IGD] Customize SerialNumber and ModelNumber

## Release v0.7.1 - 2024-01-11(10:32:52 +0000)

### Changes

- - [UPnP] UPnP port mapping automatic cleanup

## Release v0.7.0 - 2023-12-15(09:29:07 +0000)

### New

- - [UPnP-IGD] We must be able to customize the configuration of the WAN device

## Release v0.6.4 - 2023-11-29(14:45:51 +0000)

### Other

- set lla controller as default

## Release v0.6.3 - 2023-11-29(14:24:16 +0000)

### Other

- - [CD-ROUTER][UPNP][IPv6] UPnP.Device.UPnPIGD needs to be enabled after IPv6 is UP, else it won't work.

## Release v0.6.2 - 2023-11-08(07:43:46 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v0.6.1 - 2023-10-28(07:19:52 +0000)

### Changes

- CLONE - Include extensions directory in ODL

## Release v0.6.0 - 2023-10-16(10:26:48 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v0.5.1 - 2023-10-13(13:46:41 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.5.0 - 2023-09-28(14:58:46 +0000)

### New

- - [UPnP] UPnP port mapping automatic cleanup

## Release v0.4.0 - 2023-09-05(13:16:18 +0000)

### New

- - [UPnP][VZ] Expose custom UPnP parameters

## Release v0.3.9 - 2023-08-24(14:03:57 +0000)

### Fixes

- [tr181-upnp] The UPnP doesn't work when behind a CG-NAT or double NAT network

## Release v0.3.8 - 2023-08-23(12:47:19 +0000)

### Other

- Remove miniupnpd as runtime dependency

## Release v0.3.7 - 2023-08-17(12:19:07 +0000)

### Other

- Add missing runtime dependency on miniupnpd-amx

## Release v0.3.6 - 2023-08-10(17:50:17 +0000)

### Fixes

- - [miniupnpd][TR181-UPNP iGD] When changing the LAN IP address, miniupnpd is not correctly restarted

## Release v0.3.5 - 2023-07-31(08:55:03 +0000)

### Fixes

- - [tr181-upnp] addportmapping action failed

## Release v0.3.4 - 2023-06-23(10:59:03 +0000)

### Other

- - [CDROUTER][UPNP][IPv6] UPnP.Device.UPnPIGD needs to be enabled after IPv6 is UP, else it won't work.

## Release v0.3.3 - 2023-06-23(09:11:15 +0000)

### Other

- [UPnP] Move back firewall SSDP UDP port 1900 handling from tr181-upnpdiscovery to tr181-upnp

## Release v0.3.2 - 2023-05-10(07:56:44 +0000)

### Other

- make tr181-upnp compatible with version 2.3.3 of miniupnpd

## Release v0.3.1 - 2023-04-04(12:49:31 +0000)

### Changes

- - [CDROUTER][UPNP][IPv6]  UPnP.Device.UPnPIGD needs to be enabled after IPv6 is UP, else it won't work.

## Release v0.3.0 - 2023-03-24(08:50:55 +0000)

### Other

- - [prpl][tr181-upnp] UPnP.Discovery and UPnP.Description are not implemented

## Release v0.2.4 - 2023-03-09(09:19:52 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.2.3 - 2023-03-03(15:36:33 +0000)

### Changes

- Change default controller to UCI

### Other

- Add missing runtime dependency on rpcd

## Release v0.2.2 - 2023-01-12(11:18:30 +0000)

### Other

- - [prpl][tr181-upnp] miniupnpd doesn't start on lla config

## Release v0.2.1 - 2023-01-09(09:55:10 +0000)

### Fixes

- event handler filter uses regexp on string literal

## Release v0.2.0 - 2023-01-05(11:52:28 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.1.5 - 2022-12-19(15:59:51 +0000)

### Other

- - [UPNP] UPNP plugin must open the relevant ports for UPNP discovery for both ipv4 and IPv6

## Release v0.1.4 - 2022-12-09(09:32:46 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.1.3 - 2022-11-24(08:25:19 +0000)

### Other

- Add runtime dependency on miniupnp

## Release v0.1.2 - 2022-11-21(10:59:19 +0000)

### Other

- Opensource component

## Release v0.1.1 - 2022-08-25(08:25:11 +0000)

### Fixes

- implement UPnP IGD limited data model

## Release v0.1.0 - 2022-08-23(14:12:50 +0000)

### New

- implement UPnP IGD limited data model

